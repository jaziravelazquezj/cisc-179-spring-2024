Welcome to Elysium! What is your name?
{} you are a detective sent out to locate the president's daughter she's been missing for several weeks.
It's up to you now to bring her home. Good luck...
How would you like to proceed {}?
Would you like to board the train into Elysium? Yes or No
Great! You've boarded the train and arrived at Elysium.
What facilitate would you like to explore? Security or Medical?
{} enters the security station and is greeted by the Elysium security chief.
You mention to the chief you're looking for a missing girl.
The chief thinks for a moment and says, "I remember seeing a suspicious individual in the area around the mayor's office. Maybe you should check it out."
Do you investigate the mayor or the chief?
You thank the chief and head towards the mayor's office. You arrive at the mayor's office and are greeted by a secretary.
You tell the secretary about the missing girl, and she tells you that the mayor might have some information.
The secretary points you to the mayor's office at the end of the hallway.
You reach the end of the hallway and hear voices coming from the mayor's office.
What is your next course of action? Listen in or Bust in?
You press your ear against the door and listen carefully. You hear the mayor being questioned by a strange voice.
The mayor responds frantically about a group of kidnapped girls being held at the warehouse.
Once you hear about the lead you quickly make your way towards the warehouse.
You arrive at the warehouse it seems to be abandon. You search and come across containers holding the girl.
You found her! She's frighten but alive. You escape with her and return her back to the president.
You've completed the mission, but who ever was behind the kidnapping is still at large... Ending 1/6
You kick open the door, and you see the mayor talking with a suspicious man in a mask.
Before you can react you get choked from behind and pass out. YOU LOSE
You decide to stay at the security station and look for more clues.
As you look around a suspicious masked man visit the station and meets with the security chief.
You take that chance to snoop into the security chief office.
You notice on his desk a map with a storage warehouse marked, along with a pistol and a security clearance badge.
Footsteps are quickly approaching you have a chance to take either the 'gun' or 'badge'. What do you take?
You quickly swipe the gun and leave the back of the station heading to the warehouse.
You arrive at the warehouse, but it's heavily guarded. Thanks to the map you found the back entrance with only one guard.
You use the gun you acquired to threaten the guard to let you in. You find the holding cell along with the chief.
With the gun you manage to subdue the chief and release the girl.
The security chief is now under investigation for conspiracy and the girl is safely returned. Ending 2/6
You quickly swipe the badge and leave the back of the station heading to the warehouse.
You arrive at the warehouse and are freely let in due to your security badge.
You see workers loading truckloads of money into a vault and the holding cell with the missing girl.
Your security badge indicates a one time usage to unlock either the 'vault' or 'holding cell'. Which do you choices?
You use the badge and enter the vault. Bags full of money you being to smuggle out through the tunnels.
You made it out with all the money to live a wealthy lifestyle, but at the cost of a human life.
You now live with the guilt for the rest of your life... Ending 3/6
You unlock the holding cage and free the girl. As you leave to escape you are apprehended by the guards and chief.
They take you away and execute you. They cover up your death and considered missing on the news. YOU LOSE
You visit the medical center, and the doctor explains the missing girl is considered dead. He passes the medical report to you.
Death due to excessive alcohol. Should you investigate the 'bar' or 'report' back to the president?
You head over to the bar and enter the lights are dimmed you grab a seat right at the bar.
Order a 'drink' or 'ask' the bartender about the missing girl?
You down the drink and start to feel woozy.
Do you drink more or confront bartender?
You drink more and more than blackout... YOU LOSE
You grab the bartender, and he slams a bottle on your head. You wake up in the security station with a throbbing headache.
The security chief tells you cause enough trouble and plans to deport you from Elysium. YOU LOSE
Bartender says he saw a little girl with a shady figure. A masked man watches you in the corner.
Do you 'buy' him a drink or 'chase' him?
You buy him a drink and he smiles. Let's make a deal I'll give you info about the girl.
He tells you she's being held at a warehouse. I'll give you the girl if you frame the mayor and security chief for the kidnapping.
Do you take the 'offer' or 'rescue' her yourself?
You take his offer. He delivers the girl, and you report back to the president accusing the mayor and the security chief.
Several weeks pass and both the mayor and chief are charged with conspiracy and a new mysterious figure is in charge of Elysium. Ending 4/6
You choose to go recuse her yourself and manage to bring her home safely.
The masked man you attempt to arrest vanished without a trace... Ending 5/6
You chase the masked man, and he leads you to the warehouse.
You find your self quickly surrounded and beaten close to death and sent back to the president as an example... YOU LOSE
You decide to take the medical reports and conclude the case. You report back to the president, and he's extremely sadden.
He claims you are the most incompetent detective, and you are left wondering if is still alive... Ending 6/6
You sadly return to the space station but are soon kidnapped into deep space! Goodbye forever... YOU LOSE