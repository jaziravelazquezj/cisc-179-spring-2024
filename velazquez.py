import json 


# Load game data from JSON file
with open('velazquez.json') as f:
    game_data = json.load(f)

rooms = game_data["rooms"]
connections = game_data["connections"]

# Player start
current_room = "start"
visited_rooms = set()
score = 0

# Function to log messages
def log_message(message):
    with open("velazquezjazira.log", "a") as log_file:
        log_file.write(message + "\n")

# Main game loop
while True:
    room = rooms[current_room]
    print(room["challange"])
    log_message(room["challange"])

    if current_room not in visited_rooms:
        visited_rooms.add(current_room)
        score += 1

    if current_room in ["b", "c", "g", "i", "j", "l", "n", "o", "q", "r", "t", "v", "x", "z"]:
        print(f"Your knowledge score: {score}")
        log_message(f"Your knowledge score: {score}")
        command = input("Enter a command to restart: ")
        log_message(f"Command entered: {command}")
        if command == "restart":
            current_room = "start"
            visited_rooms = set()
            score = 0
            print("Game restarted.")
            log_message("Game restarted.")
        else:
            print("Invalid command. Game will restart.")
            log_message("Invalid command. Game will restart.")
            current_room = "start"
            visited_rooms = set()
            score = 0
            print("Game restarted.")
            log_message("Game restarted.")
        continue

    command = input("Enter a command: ")
    log_message(f"Command entered: {command}")

    if command in room["commands"]:
        if command in connections[current_room]:
            current_room = connections[current_room][command]
    else:
        print("Invalid command.")
        log_message("Invalid command.")

# Save game log
game_log = {
    "score": score,
    "visited_rooms": list(visited_rooms)
}



